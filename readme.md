# TOY Banners

Review site:
https://toyota1.pages.broco.work/banners/

## Starting a new banner project.

1. Duplicate the `/_template/` folder and rename it with the job number of your project. *If the campaign has multiple designs/sets, add an additional name to file path for specificity.*
1. Add the path to the `.gitlab-ci.yml` file.
1. Create your review site using the `$ gulp review` command in within your folder.
1. Commit/merge your changes into the `master` branch, and your code will automatically be deployed.
